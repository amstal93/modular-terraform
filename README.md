# Modular-Terraform

Project contains terraform modular code.

# Code information
## Modules
- modules folder contains all the resources that were written separately in their own main.tf files
- Each module (ex. ec2) has its own main.tf, variables.tf, output.tf files in it.
- declared variables can be passed from the terraform invocation.

## Environments
- variables.tfvars can be used for each environment (ex. Dev)
- Run `terraform plan -var-file=./environments/Dev/variables.tf` to pass the variables.
