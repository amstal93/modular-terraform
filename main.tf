provider "aws" {
  region = "us-east-1"
}

variable "instance_ami" {}
variable "instance_type" {}